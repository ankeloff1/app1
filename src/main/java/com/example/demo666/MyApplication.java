package com.example.demo666;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@ConfigurationProperties("my")
public class MyApplication {
    private String name;
    private String surname;
    private String father;
}
