package com.example.demo666;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Arrays;

@SpringBootApplication
public class Demo666Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo666Application.class, args);
    }

}
