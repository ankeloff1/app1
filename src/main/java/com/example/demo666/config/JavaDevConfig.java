package com.example.demo666.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

@Profile("dev")
@Configuration
public class JavaDevConfig {

    @PostConstruct
    public void test(){
        System.out.println("Loaded dev environment");
    }

    @PostConstruct
    public void test2(){
        System.out.println("loaded dev environment");
    }
}
