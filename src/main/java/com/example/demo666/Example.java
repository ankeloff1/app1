package com.example.demo666;

import ch.qos.logback.classic.Level;
//import org.slf4j.Logger;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

public class Example {

    private static final Logger logger
            = (Logger) LoggerFactory.getLogger(Example.class);

    public static void main(String[] args) {
        ch.qos.logback.classic.Logger parentLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.bealdung.logback");
        parentLogger.setLevel(Level.INFO);

        Logger childlogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.baeldung.logback.tests");

        parentLogger.warn("This message is logged because WARN > INFO.");
        parentLogger.debug("This message is not logged because DEBUG < INFO.");
        childlogger.info("INFO == INFO");
        childlogger.debug("DEBUG < INFO");
        logger.info("Example log from {}", Example.class.getSimpleName());

        Logger logger =
                (ch.qos.logback.classic.Logger)LoggerFactory.getLogger("com.baeldung.logback");
        logger.debug("Hi there!");

        Logger rootLogger =
                (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.debug("This message is logged because DEBUG == DEBUG.");

        rootLogger.setLevel(Level.ERROR);

        logger.warn("This message is not logged because WARN < ERROR.");
        logger.error("This is logged.");

    }

}
