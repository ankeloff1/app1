package com.example.demo666;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/rest/hello")
@RestController
@RequiredArgsConstructor
public class HelloResource {

    private final MyApplication myApplication;

    @Value("${spring.message}")
    private String message;

    @GetMapping
    public String hello(){
        return message;
    }

    @GetMapping("/w")
    public String get(){

        return myApplication.getName() + " " + myApplication.getSurname() + " " + myApplication.getFather();
    }
}
